var exercise_util = Object();

exercise_util.context_root = '';

exercise_util.load_select_items = function () {
    $.get(exercise_util.sendto('/equipment'), function (data, status) {

        if (status === 'success') {

            var opts = undefined;
            for (var i = 0; i < data.length; i++) {

                opts +=  '<option value="' + data[i]['id'] + '">' + data[i]['name'] + '</option>';
            }

            $('#equipment').html(opts);
        }
    })
};

exercise_util.submit_report = function () {
    var exercise_id = $("#exercise_id").attr("value");
    var set = $("#set").val();
    var approaches = $("#approaches").val();
    var equipment_id = $("#equipment").val();

    var report = Object();
    report.exercise = Object();
    report.exercise.id = exercise_id;
    report.set = set;
    report.approaches = approaches;
    report.equipment = Object();
    report.equipment.id = equipment_id;

    $.ajax({
        url: exercise_util.sendto('/workouts'),
        type: 'POST',
        data: JSON.stringify(report),
        contentType: 'application/json; charset=utf-8',
        success: function (data, textStatus, jqXHR) {
            var redirect_url = 'http://'
                .concat(window.location.host)
                .concat(exercise_util.sendto('exercises'));
            window.location.replace(redirect_url);
        }
    });
};

exercise_util.sendto = function (relative_url) {
    if (exercise_util.context_root) {
        return exercise_util.context_root.concat('/').concat(relative_url);
    } else {
        console.log('Context root undefined!')
    }
};
