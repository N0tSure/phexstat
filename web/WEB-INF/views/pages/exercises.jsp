<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Physical Exercises Statistic - Exercises</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="${pageContext.request.contextPath}/">PhExStat</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="${pageContext.request.contextPath}/exercises">Exercises</a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/exercises">Some another link</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="jumbotron">
            <h2>Перед тренировкой</h2>
            <p>
                Не забывай, что при выполнении любых спортивных упражнений должны соблюдаться определенные условия.
                Чтобы накачать мышцы правильно — их нужно разогреть. Помимо этого, делая упражнения, важно придерживаться техники.
                Ощущаешь нагрузку только в нужных мышцах? Значит ты все делаешь правильно.
            </p>
            <p>
                По количеству самой нагрузки — все индивидуально. Новичкам, как правило, рекомендуется делать два-три подхода,
                повторяя движения 6 – 10 раз. Вес гантелей можно прикинуть так: свободно подтягиваешься 8 раз по три подхода —
                первое время твоим бецепсам/трицепсам подойдут гантели весом 8-10 кг, нет — бери меньше. В любом случае со временем
                тебе понадобится увеличивать вес, поэтому самый распространенный вариант для домашних тренировок с гантелями — это
                покупка разборных гантелей с подходящим набором дисков к ним.
            </p>
        </div>
        <div class="panel-default">
            <div class="panel-heading">
                <h2>Упражнения</h2>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    <c:forEach items="${exercises}" var="exercise">
                        <a class="list-group-item" href="exercises/<c:out value="${exercise.id}" /> ">
                            <h4 class="list-group-item-heading">
                                <c:out value="${exercise.name}" />
                            </h4>
                            <p class="list-group-item-text">
                                <span class="text-info">Группа:  </span><c:out value="${exercise.exerciseType.localName}" />
                            </p>
                        </a>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
