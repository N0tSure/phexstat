<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Physical Exercises Statistic - Do Exercise</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/exercise.js"></script>
    <script>
        exercise_util.context_root = '${pageContext.request.contextPath}';
    </script>
</head>
<body onload="exercise_util.load_select_items()">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="${pageContext.request.contextPath}/">PhExStat</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="${pageContext.request.contextPath}/exercises">Exercises</a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/exercises">Some another link</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div>
                <h2>
                    <c:out value="${exercise.name}" />
                </h2>
            </div>
            <div class="col-lg-6">
                <img
                        src="${pageContext.request.contextPath}/images/<c:out value="${exercise.pictureFileName}" />"
                        class="img-thumbnail"
                        width="100%"
                        height="100%"
                        alt="<c:out value="${exercise.name}" />"
                >
            </div>
            <div class="col-lg-6">
                <p>
                    <c:out value="${exercise.description}" />
                </p>
                <form>
                    <div class="form-group">
                        <label for="set">Set</label>
                        <input hidden id="exercise_id" type="text" value="<c:out value="${exercise.id}" />">
                        <input class="form-control" id="set" type="number">
                    </div>
                    <div class="form-group">
                        <label for="approaches">Approaches:</label>
                        <input class="form-control" id="approaches" type="number">
                    </div>
                    <div class="form-group">
                        <label for="equipment">Equipment</label>
                        <select class="form-control" id="equipment"></select>
                    </div>
                    <a href="${pageContext.request.contextPath}/exercises" class="btn btn-default" role="button">
                        Назад
                    </a>
                    <button class="btn btn-primary" type="button" onclick="exercise_util.submit_report()">
                        Отправить
                    </button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
