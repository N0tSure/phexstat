<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Physical Exercises Statistic - Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="${pageContext.request.contextPath}/">PhExStat</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="${pageContext.request.contextPath}/exercises">Exercises</a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/exercises">Some another link</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="jumbotron">
        <h1>Physical Exercises Statistic</h1>
        <p>
            Provides your own dairy of physical exercises
        </p>
    </div>
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2>All workout reports</h2>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Exercise</th>
                            <th>Set</th>
                            <th>Approaches</th>
                            <th>Measure</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${workouts}" var="workout">
                            <tr id="workout_<c:out value="${workout.id}"/>">
                                <th><c:out value="${workout.date}" /></th>
                                <th><c:out value="${workout.exercise.name}" /></th>
                                <th><c:out value="${workout.set}" /></th>
                                <th><c:out value="${workout.approaches}" /></th>
                                <th>
                        <span>
                            <c:out value="${workout.equipment.value}" />
                            <c:out value="${workout.equipment.measure}" />.
                        </span>
                                </th>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>