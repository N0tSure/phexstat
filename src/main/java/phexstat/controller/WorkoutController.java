package phexstat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import phexstat.model.*;
import phexstat.repository.EquipmentRepository;
import phexstat.repository.ExerciseRepository;
import phexstat.repository.WorkoutRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * Created on 28.07.2017.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
@Controller
@RequestMapping(path = "/workouts")
public class WorkoutController {

    private final EquipmentRepository equipmentRepository;
    private final ExerciseRepository exerciseRepository;
    private final WorkoutRepository workoutRepository;

    @Autowired
    public WorkoutController(
            EquipmentRepository equipmentRepository,
            ExerciseRepository exerciseRepository,
            WorkoutRepository workoutRepository
    ) {
        this.equipmentRepository = equipmentRepository;
        this.exerciseRepository = exerciseRepository;
        this.workoutRepository = workoutRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getWorkouts(Model model) {
        model.addAttribute("workouts", workoutRepository.findAll());
        return "/pages/workouts";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String saveWorkoutReport(@RequestBody Workout workout) {
        workout.setDate(new Date());
        workout.setEquipment(equipmentRepository.findOne(workout.getEquipment().getId()));
        workout.setExercise(exerciseRepository.findOne(workout.getExercise().getId()));

        workoutRepository.save(workout);
        return "redirect:/exercises";
    }
}
