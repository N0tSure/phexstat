package phexstat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import phexstat.model.Equipment;
import phexstat.repository.EquipmentRepository;

import java.util.List;

/**
 * <p>
 * Created on 10.08.2017.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
@Controller
@RequestMapping(path = "/equipment")
public class EquipmentController {

    private final EquipmentRepository equipmentRepository;

    @Autowired
    public EquipmentController(EquipmentRepository equipmentRepository) {
        this.equipmentRepository = equipmentRepository;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Iterable<Equipment>> getEquipment() {
        return new ResponseEntity<>(equipmentRepository.findAll(), HttpStatus.OK);
    }
}
