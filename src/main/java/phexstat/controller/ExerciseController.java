package phexstat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import phexstat.repository.ExerciseRepository;

/**
 * Created on 04 Aug, 2017.
 *
 * @author Artemis A. Sirosh
 */
@Controller
@RequestMapping(path = "/exercises")
public class ExerciseController {

    private final ExerciseRepository exerciseRepository;

    @Autowired
    public ExerciseController(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getExercisesView(Model model) {
        model.addAttribute("exercises", exerciseRepository.findAll());
        return "/pages/exercises";
    }


    @RequestMapping(value = "/{exerciseId}", method = RequestMethod.GET)
    public String getOneExercise(@PathVariable("exerciseId") long id, Model model) {
        model.addAttribute("exercise", exerciseRepository.findOne(id));
        return "/pages/exercise";
    }
}
