package phexstat.model;

import static com.google.common.base.Objects.*;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.util.Date;

/**
 * Created on 31 May, 2017.
 *
 * <p>
 *     Model of workout result, field description:
 *     <ul>
 *         <li>
 *             {@link #exercise} according {@link Exercise}
 *         </li>
 *         <li>
 *             {@link #set} amount of executed exercises
 *         </li>
 *         <li>
 *             {@link #approaches} amount of executed set
 *         </li>
 *         <li>
 *             {@link #equipment} equipment used for exercise
 *         </li>
 *         <li>
 *             {@link #date} date of workout execution
 *         </li>
 *     </ul>
 *
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class Workout {

    private Long id;
    private Exercise exercise;
    private int set;
    private int approaches;
    private Equipment equipment;
    private Date date;

    public Workout() {
    }

    public Workout(Exercise exercise, int set, int approaches, Equipment equipment, Date date) {
        this.exercise = exercise;
        this.set = set;
        this.approaches = approaches;
        this.equipment = equipment;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public int getSet() {
        return this.set;
    }

    public int getApproaches() {
        return approaches;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public Date getDate() {
        return date;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public void setSet(int set) {
        this.set = set;
    }

    public void setApproaches(int approaches) {
        this.approaches = approaches;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object that) {
        return this == that ||
                (
                        !equal(null, that) &&
                                that instanceof Workout &&
                                equal(this.id, ((Workout) that).id) &&
                                equal(this.exercise, ((Workout) that).exercise) &&
                                equal(this.set, ((Workout) that).set) &&
                                equal(this.approaches, ((Workout) that).approaches) &&
                                equal(this.equipment, ((Workout) that).equipment) &&
                                equal(this.date, ((Workout) that).date)
                );
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, exercise, set, approaches, equipment, date);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", this.id)
                .add("exercise", this.exercise)
                .add("set", this.set)
                .add("approaches", this.approaches)
                .add("equipment", this.equipment)
                .add("date", String.format("%1$td.%1$tm.%1$ty", this.date))
                .toString();
    }
}
