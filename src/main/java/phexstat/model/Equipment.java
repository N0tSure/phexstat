package phexstat.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import static com.google.common.base.Objects.equal;

/**
 * Created on 28 Jun, 2017.
 * Represent one sport equipment unit
 *
 * @author Artemis A. Sirosh
 */
public class Equipment {

    private Long id;
    private String name;
    private EquipmentType type;
    private String measure;
    private Double value;

    public Equipment() {

    }

    public Equipment(String name, EquipmentType type, String measure, Double value) {
        this.name = name;
        this.type = type;
        this.measure = measure;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public EquipmentType getType() {
        return type;
    }

    public String getMeasure() {
        return measure;
    }

    public Double getValue() {
        return value;
    }

    public void setType(EquipmentType type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name, type, measure, value);
    }

    @Override
    public boolean equals(Object that) {
        return this == that ||
                (
                        !equal(null, that) &&
                                that instanceof Equipment &&
                                equal(this.id, ((Equipment) that).id) &&
                                equal(this.name, ((Equipment) that).name) &&
                                equal(this.type, ((Equipment) that).type) &&
                                equal(this.measure, ((Equipment) that).measure) &&
                                equal(this.value, ((Equipment) that).value)
                );
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", this.id)
                .add("name", this.name)
                .add("type", this.type)
                .add("measure", this.measure)
                .add("value", this.value)
                .toString();
    }

}
