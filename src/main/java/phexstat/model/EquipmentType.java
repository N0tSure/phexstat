package phexstat.model;

import com.google.common.base.MoreObjects;

/**
 * Created on 28 Jun, 2017.
 *
 * @author Artemis A. Sirosh
 */
public enum EquipmentType {

    DUMBBELL("Гантель");

    private final String localName;

    EquipmentType(String localName) {
        this.localName = localName;
    }

    public String getLocalName() {
        return localName;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", this.name())
                .add("local name", this.localName)
                .toString();
    }
}
