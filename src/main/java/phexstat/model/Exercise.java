package phexstat.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import static com.google.common.base.Objects.*;



/**
 * Created on 31 May, 2017.
 * Model of exercise
 * @author Artemis A. Sirosh
 */
public class Exercise {

    private Long id;
    private String name;
    private ExerciseType exerciseType;
    private EquipmentType equipmentType;
    private String description;
    private String pictureFileName;


    public Exercise() {
    }

    public Exercise(String name, ExerciseType exerciseType, EquipmentType equipmentType, String description, String pictureFileName) {
        this.name = name;
        this.exerciseType = exerciseType;
        this.equipmentType = equipmentType;
        this.description = description;
        this.pictureFileName = pictureFileName;
    }

    public void setExerciseType(ExerciseType exerciseType) {
        this.exerciseType = exerciseType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPictureFileName(String pictureFileName) {
        this.pictureFileName = pictureFileName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public ExerciseType getExerciseType() {
        return exerciseType;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public String getName() {
        return name;
    }

    public String getPictureFileName() {
        return pictureFileName;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object that) {
        return this == that ||
                (
                        !equal(null, that) &&
                                that instanceof Exercise &&
                                equal(getId(), ((Exercise) that).getId()) &&
                                equal(getName(), ((Exercise) that).getName()) &&
                                equal(getExerciseType(), ((Exercise) that).getExerciseType()) &&
                                equal(getEquipmentType(), ((Exercise) that).getEquipmentType()) &&
                                equal(getDescription(), ((Exercise) that).getDescription()) &&
                                equal(getPictureFileName(), ((Exercise) that).getPictureFileName())
                );
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name, exerciseType, equipmentType, description, pictureFileName);
    }

    @Override
    public String toString() {
        return MoreObjects
                .toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("exercise type", exerciseType)
                .add("equipment type", equipmentType)
                .add("desc length", description != null ? description.length() : 0)
                .add("picture", pictureFileName)
                .toString();
    }
}
