package phexstat.model.deserializer;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import phexstat.model.Exercise;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;

/**
 * Created on 14 Aug, 2017.
 * <p>
 *     Deserializes exercises from xml files
 * </p>
 * @author Artemis A. Sirosh
 */
class ExerciseDeserializer extends AbstractXMLModelDeserializer<Exercise> {

    ExerciseDeserializer(
            XmlMapper mapper, XMLInputFactory inputFactory, FileInputStream inputStream) throws XMLStreamException {
        super(Exercise.class, mapper, inputFactory, inputStream);
    }
}
