package phexstat.model.deserializer;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.common.base.Preconditions;
import phexstat.model.Equipment;
import phexstat.model.Exercise;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Created on 13 Aug, 2017.
 * <p>
 *     Factory for model deserializers creation
 * </p>
 * @author Artemis A. Sirosh
 */
public class ModelDeserializerFactory {

    private final XmlMapper mapper;
    private final XMLInputFactory inputFactory;

    public ModelDeserializerFactory(XmlMapper mapper, XMLInputFactory inputFactory) {
        this.mapper = requireNonNull(mapper, "null pointer given as XmlMapper");
        this.inputFactory = requireNonNull(inputFactory, "null pointer given as XMLInputFactory");
    }

    /**
     * Creates {@link EquipmentDeserializer} from given file
     * @param serializedEquipment xml file with equipment
     * @return {@link ModelDeserializer} implementation
     * @throws RuntimeException if something wrong due creation occur
     * @throws NullPointerException if given null pointer as {@code serializedEquipment}
     */
    public ModelDeserializer<Equipment> createEquipmentDeserializer(File serializedEquipment) {
        try {
            return new EquipmentDeserializer(mapper, inputFactory, openFile(serializedEquipment));
        } catch (IOException | XMLStreamException exc) {
            throw new RuntimeException("Something wrong due ModelDeserializer creation", exc);
        }
    }

    /**
     * Creates {@link ExerciseDeserializer} from given file
     * @param serializedExercise xml file with exercises
     * @return {@link ModelDeserializer} implementation
     * @throws RuntimeException if something wrong due creation occur
     * @throws NullPointerException if given null pointer as {@code serializedExercise}
     */
    public ModelDeserializer<Exercise> createExerciseDeserializer(File serializedExercise) {
        try {
            return new ExerciseDeserializer(mapper, inputFactory, openFile(serializedExercise));
        } catch (IOException | XMLStreamException exc) {
            throw new RuntimeException("Something wrong due ModelDeserializer creation", exc);
        }
    }

    private FileInputStream openFile(File file) throws IOException {
        return new FileInputStream(requireNonNull(file, "null pointer given as XML File"));
    }
}
