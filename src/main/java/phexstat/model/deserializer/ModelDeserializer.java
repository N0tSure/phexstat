package phexstat.model.deserializer;

/**
 * Created on 13 Aug, 2017.
 * <p>
 *     Deserializer for models, from xml format.
 * </p>
 * @author Artemis A. Sirosh
 */
public interface ModelDeserializer<Model> extends AutoCloseable {

    /**
     * Check if source have next model, supply next tag
     * @return {@code true} if has next model
     */
    boolean hasNextTag();

    /**
     * Deserialize model from tag
     * @return parsed from tag model
     */
    Model nextModel();
}
