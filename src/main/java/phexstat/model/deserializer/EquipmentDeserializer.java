package phexstat.model.deserializer;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import phexstat.model.Equipment;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;

/**
 * Created on 14 Aug, 2017.
 * <p>
 *     Deserializes equipment units from xml file
 * </p>
 * @author Artemis A. Sirosh
 */
class EquipmentDeserializer extends AbstractXMLModelDeserializer<Equipment> {

    EquipmentDeserializer(
            XmlMapper mapper, XMLInputFactory inputFactory, FileInputStream inputStream) throws XMLStreamException {
        super(Equipment.class, mapper, inputFactory, inputStream);
    }
}
