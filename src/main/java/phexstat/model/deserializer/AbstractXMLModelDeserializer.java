package phexstat.model.deserializer;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created on 13 Aug, 2017.
 * <p>
 *     Deserializer implementation, construct skip first declaration
 *     line of xml: {@code <?xml version='1.0' encoding='UTF-8'?>}.<br>
 *     Also skip first tag element of xml file
 * </p>
 * @author Artemis A. Sirosh
 */
abstract class AbstractXMLModelDeserializer<Model> implements ModelDeserializer<Model> {

    private final Class<Model> modelType;
    private final XmlMapper mapper;
    private final XMLStreamReader reader;

    AbstractXMLModelDeserializer(
            Class<Model> modelType,
            XmlMapper mapper,
            XMLInputFactory inputFactory,
            FileInputStream inputStream) throws XMLStreamException {
        this.modelType = modelType;
        this.mapper = mapper;
        this.reader = inputFactory.createXMLStreamReader(inputStream);
        this.reader.next();
    }

    /**
     * {@inheritDoc}
     *
     * @return true if next tag code equals
     * {@link XMLStreamConstants#START_ELEMENT}
     * @see XMLStreamReader#nextTag()
     * @throws RuntimeException if exception occur due next tag retrieving
     */
    @Override
    public boolean hasNextTag() {
        try {
            return Integer.compare(this.reader.nextTag(), XMLStreamConstants.START_ELEMENT) == 0;
        } catch (XMLStreamException exc) {
            throw new RuntimeException(exc);
        }
    }

    /**
     * {@inheritDoc}
     * Deserialize next model
     * @return deserialized model
     * @throws RuntimeException if due parsing XML tag exception
     * will thrown
     */
    @Override
    public Model nextModel() {
        try {
            return mapper.readValue(reader, modelType);
        } catch (IOException exc) {
            throw new RuntimeException(exc);
        }
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }
}
