package phexstat.model.dumper;

import com.google.common.base.Preconditions;
import phexstat.model.Workout;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkState;

/**
 * <p>
 * Created on 14.08.2017.
 * </p>
 * <p>
 *     Create CSV file in parent directory, and dumps
 *     given {@link Workout}
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class WorkoutCSVDumper implements Dumper<Workout> {

    private final static String WORKOUT_HEADER = "%1$s,%2$s,%3$s,%4$s,%5$s,%6$s\n";
    private final static String WORKOUT_ROW_FORMAT = "%1$d,%2$td.%2$tm.%2$tY,%3$d,%4$d,%5$d,%6$d\n";
    private final PrintWriter writer;

    public WorkoutCSVDumper(File parent) throws IOException {

        File file = new File(parent, String.format("%s_workouts.csv", UUID.randomUUID()));
        checkState(file.createNewFile(), "Unable to create output file");
        this.writer = new PrintWriter(file);
        this.writer.format(WORKOUT_HEADER, "ID", "Date", "Exercise", "Equipment", "Approaches", "Set");

    }

    @Override
    public void dumpModel(Workout workout) throws IOException {
        this.writer.format(
                WORKOUT_ROW_FORMAT,
                workout.getId(),
                workout.getDate(),
                workout.getExercise().getId(),
                workout.getEquipment().getId(),
                workout.getApproaches(),
                workout.getSet()
                );
    }

    /**
     * {@inheritDoc}
     * @throws Exception
     */
    @Override
    public void close() throws Exception {
        writer.close();
    }
}
