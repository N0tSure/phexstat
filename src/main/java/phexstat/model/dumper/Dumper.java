package phexstat.model.dumper;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * <p>
 * Created on 14.08.2017.
 * </p>
 * <p>
 *     Dumps models to file
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public interface Dumper<Model> extends AutoCloseable {

    /**
     * Write given model in file
     * @param model {@code Model} object, which will written to file
     * @throws IOException if due writing file occur problem
     */
    void dumpModel(Model model) throws IOException;
}
