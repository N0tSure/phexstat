package phexstat.model.sequence;

import phexstat.model.Exercise;

/**
 * Created on 31 May, 2017.
 *
 * <p>
 *     Model of group of exercises, strained into row
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public interface WorkoutSequence extends Iterable<Exercise> {

    /**
     * Returns current exercise, and leave this in sequence
     * @return current {@link Exercise}
     */
    Exercise currentExercise();

    /**
     * Returns next exercise, and remove this one from sequence
     * @return next {@link Exercise}
     */
    Exercise nextExercise();

    /**
     * Provides checking of end of sequence
     * @return true if at least one element yet existed in sequence
     */
    boolean hasNextExercise();

}
