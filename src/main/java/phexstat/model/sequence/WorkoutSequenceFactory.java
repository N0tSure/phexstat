package phexstat.model.sequence;

/**
 * Created on 31 May, 2017.
 * <p>
 *     This factory creates vast implementations of {@link WorkoutSequence}
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class WorkoutSequenceFactory {

    public WorkoutSequenceFactory() {

    }

    public WorkoutSequence getAutoLoadingSequence(int amount) {
        return new AutoLoadingWorkoutSequence(amount);
    }

}
