package phexstat.model.sequence;

import phexstat.model.Exercise;
import phexstat.model.ExerciseType;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Stream;

/**
 * Created on 31 May, 2017.
 * This auto loading exercise sequence container for test purposes
 * @author Artemis A. Sirosh
 */
class AutoLoadingWorkoutSequence implements WorkoutSequence {
    private static final String[] EXERCISES_NAMES =
            "foo bar baz qux quux corge grault garply waldo fred plugh".split(" ");

    private Queue<Exercise> exercises;

    AutoLoadingWorkoutSequence(int amount) {

        this.exercises = new LinkedList<>();
        Random random = new Random(42);

        this.exercises = Stream.generate(() -> {
            String name = EXERCISES_NAMES[random.nextInt(EXERCISES_NAMES.length)];
            Exercise exercise = new Exercise();
            exercise.setName(name);
            exercise.setExerciseType(ExerciseType.values()[random.nextInt(ExerciseType.values().length)]);
            exercise.setDescription("Auto generated exercise: " + name);
            exercise.setPictureFileName(name + ".jpg");
            return exercise;
        }).limit(amount).collect(Collector.of(
                LinkedList::new,
                List::add,
                (left, right) -> {left.addAll(right); return left; },
                Collector.Characteristics.IDENTITY_FINISH
        ));

    }

    @Override
    public Exercise currentExercise() {
        return exercises.peek();
    }

    @Override
    public Exercise nextExercise() {
        return exercises.remove();
    }

    @Override
    public boolean hasNextExercise() {
        return !exercises.isEmpty();
    }

    @Override
    public Iterator<Exercise> iterator() {
        return exercises.iterator();
    }
}
