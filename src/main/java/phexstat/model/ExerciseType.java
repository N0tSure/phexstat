package phexstat.model;

import com.google.common.base.MoreObjects;

/**
 * Created on 28 Jun, 2017.
 *
 * @author Artemis A. Sirosh
 */
public enum  ExerciseType {

    BICEPS("Упражнения на бицепс"),
    TRICEPS("Упражнения на трицепс"),
    SHOULDERS("Упражнения на мышцы плеч"),
    CHEST("Упражнения на мышцы груди"),
    BACK("Упражнения на мышцы спины"),
    PRESS("Упражнения на мышцы пресса"),
    RUNNING("Бег");

    private final String localName;

    ExerciseType(String localName) {
        this.localName = localName;
    }

    public String getLocalName() {
        return localName;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", this.name())
                .add("local name", this.localName)
                .toString();
    }
}
