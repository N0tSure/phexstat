package phexstat.repository;

import phexstat.model.Equipment;

/**
 * <p>
 * Created on 27.07.2017.
 * </p>
 * <p>
 *     Repository for {@link phexstat.model.Equipment} model
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public interface EquipmentRepository extends CrudRepository<Equipment, Long> {
}
