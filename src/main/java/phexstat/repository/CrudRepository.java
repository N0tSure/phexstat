package phexstat.repository;

/**
 * Created on 14 Jun, 2017.
 *
 * <p>
 *     Model of CrudRepository, similar to Spring Framework CrudRepository
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public interface CrudRepository<T, ID> {

    /**
     * Saves model
     * @param model which will saved
     * @return saved model
     */
    T save(T model);

    /**
     * Save a few models
     * @param models which will saved
     * @return models that has been saved
     */
    Iterable<T> save(Iterable<T> models);

    /**
     * Find one model by id
     * @param id is id of model
     * @return founded model or null
     */
    T findOne(ID id);

    /**
     * Find all models
     * @return all available models
     */
    Iterable<T> findAll();

    /**
     * Find models by given ids
     * @param ids is ids of model, which will be
     *           used to searching models
     * @return founded models
     */
    Iterable<T> findAll(Iterable<ID> ids);

    /**
     * Deletes one model by id
     * @param id is id of model, which will be removed
     */
    void delete(ID id);

    /**
     * Deletes all models
     */
    void deleteAll();

    /**
     * Check for model with given id existence
     * @param id is id of model
     * @return true if model existing
     */
    boolean exists(ID id);

    /**
     * Count available models
     * @return number of models
     */
    long count();

}
