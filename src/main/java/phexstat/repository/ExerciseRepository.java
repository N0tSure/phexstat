package phexstat.repository;

import phexstat.model.Exercise;

/**
 * <p>
 *      Created on 27.07.2017.
 * </p>
 * <p>
 *     Repository for {@link phexstat.model.Exercise} model
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public interface ExerciseRepository extends CrudRepository<Exercise, Long> {
}
