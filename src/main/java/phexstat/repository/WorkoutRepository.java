package phexstat.repository;

import phexstat.model.Workout;

/**
 * Created on 14 Jun, 2017.
 *
 * <p>
 *     Repository for {@link Workout} model
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public interface WorkoutRepository extends CrudRepository<Workout, Long> {
}
