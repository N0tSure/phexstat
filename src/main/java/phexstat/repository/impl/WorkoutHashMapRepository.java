package phexstat.repository.impl;

import phexstat.model.Workout;
import phexstat.model.dumper.Dumper;
import phexstat.model.dumper.WorkoutCSVDumper;
import phexstat.repository.WorkoutRepository;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.Objects.requireNonNull;

/**
 * Created on 14 Jun, 2017.
 *
 * <p>
 *     Implementation of {@link WorkoutRepository}, based on
 *     {@link java.util.HashMap} as storage
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class WorkoutHashMapRepository extends HashMapCrudRepository<Workout, Long> implements WorkoutRepository {

    private File dumpDir;

    public WorkoutHashMapRepository() {
        super(() -> Math.round(Math.random() * 100_000), Workout.class);
    }

    public WorkoutHashMapRepository(File dumpDir) {
        super(() -> Math.round(Math.random() * 100_000), Workout.class);
        this.dumpDir = dumpDir;
    }

    /**
     * Dumps workout reports to CSV file
     * @throws Exception if problem due IO operation occur,
     * or if {@code dumpDir} is null
     */
    public void destroy() throws Exception {
        try (Dumper<Workout> dumper = new WorkoutCSVDumper(requireNonNull(dumpDir, "dump folder file is null"))) {
            StreamSupport.stream(this.findAll().spliterator(), false)
                    .sorted(Comparator.comparingLong(Workout::getId))
                    .forEach(workout -> {
                        try {
                            dumper.dumpModel(workout);
                        } catch (IOException exc) {
                            throw new RuntimeException(exc);
                        }
                    });
        }
    }

}
