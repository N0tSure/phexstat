package phexstat.repository.impl;

import phexstat.repository.CrudRepository;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created on 14 Jun, 2017.
 *
 * <p>
 *     Based on HashMap implementation of {@link CrudRepository}
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public abstract class HashMapCrudRepository<T, ID> implements CrudRepository<T, ID> {

    private Map<ID, T> database;
    private Supplier<ID> idGenerator;
    private Class<T> modelType;

    protected HashMapCrudRepository(Supplier<ID> idGenerator, Class<T> modelType) {
        this.database = new HashMap<>();
        this.idGenerator = idGenerator;
        this.modelType = modelType;
    }

    protected HashMapCrudRepository(Supplier<ID> idGenerator, Class<T> modelType, HashMap<ID, T> preLoadedDatabase) {
        this.database = preLoadedDatabase;
        this.idGenerator = idGenerator;
        this.modelType = modelType;
    }

    @Override
    public T save(T model) {

        ID id = getIDFromModel(model);

        if (id == null) {
            id = idGenerator.get();
            setIDIntoModel(model, id);
        }

        database.put(id, model);
        return model;
    }

    @Override
    public Iterable<T> save(Iterable<T> models) {
        return StreamSupport.stream(models.spliterator(), false)
                .map(model -> {
                    if (getIDFromModel(model) != null) {
                        return database.get(getIDFromModel(model));
                    } else {
                        ID id = idGenerator.get();
                        setIDIntoModel(model, id);
                        database.put(id, model);
                        return model;
                    }
                }).collect(Collectors.toList());
    }

    @Override
    public T findOne(ID id) {
        return database.get(id);
    }

    @Override
    public Iterable<T> findAll() {
        return database.values();
    }

    @Override
    public Iterable<T> findAll(Iterable<ID> ids) {
        return StreamSupport.stream(ids.spliterator(), false)
                .map(id -> database.get(id))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(ID id) {
        database.remove(id);
    }

    @Override
    public void deleteAll() {
        database.clear();
    }

    @Override
    public boolean exists(ID id) {
        return database.containsKey(id);
    }

    @Override
    public long count() {
        return database.size();
    }

    /**
     * Extract id field from model using Reflection API
     * @param model instance of T
     * @return identifier instance
     * @throws RuntimeException if could not find id field
     * @throws ClassCastException if identifier has unexpected type
     */
    @SuppressWarnings("unchecked")
    private ID getIDFromModel(T model) {
        try {
            Field idField = modelType.getDeclaredField("id");
            idField.setAccessible(true);
            return (ID) idField.get(model);

        } catch (NoSuchFieldException | IllegalAccessException exc) {
            throw new RuntimeException("Something wrong with model", exc);
        }
    }

    /**
     * Set id in model using Reflection API
     * @param model instance of T
     * @param id identifier
     * @throws RuntimeException if could not find id field
     */
    private void setIDIntoModel(T model, ID id) {

        try {
            Field idField = modelType.getDeclaredField("id");
            idField.setAccessible(true);
            idField.set(model, id);
        } catch (NoSuchFieldException | IllegalAccessException exc) {
            throw new RuntimeException("Something wrong with model", exc);
        }

    }

}
