package phexstat.repository.impl;

import phexstat.model.Exercise;
import phexstat.repository.ExerciseRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * <p>
 * Created on 27.07.2017.
 * </p>
 * <p>
 *     Implements {@link phexstat.repository.ExerciseRepository},
 *     based on HashMap storage
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class ExerciseHashMapRepository extends HashMapCrudRepository<Exercise, Long> implements ExerciseRepository {

    public ExerciseHashMapRepository() {
        super(() -> Math.round(Math.random() * 100_000), Exercise.class);
    }

    public ExerciseHashMapRepository(Map<Long, Exercise> preLoadedDatabase) {
        super(() -> Math.round(Math.random() * 100_000), Exercise.class, new HashMap<>(preLoadedDatabase));
    }
}
