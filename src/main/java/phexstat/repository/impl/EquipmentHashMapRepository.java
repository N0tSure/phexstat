package phexstat.repository.impl;

import phexstat.model.Equipment;
import phexstat.repository.EquipmentRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * <p>
 * Created on 27.07.2017.
 * </p>
 * <p>
 *     Implementation of {@link phexstat.repository.EquipmentRepository} based on
 *     {@link java.util.HashMap} as storage
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class EquipmentHashMapRepository extends HashMapCrudRepository<Equipment, Long> implements EquipmentRepository {

    public EquipmentHashMapRepository() {
        super(() -> Math.round(Math.random() * 100_000), Equipment.class);
    }

    public EquipmentHashMapRepository(Map<Long, Equipment> preLoadedDatabase) {
        super(() -> Math.round(Math.random() * 100_000), Equipment.class, new HashMap<>(preLoadedDatabase));
    }
}
