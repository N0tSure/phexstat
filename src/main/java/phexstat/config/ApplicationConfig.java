package phexstat.config;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import gherkin.lexer.Ru;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import phexstat.model.*;
import phexstat.model.deserializer.ModelDeserializer;
import phexstat.model.deserializer.ModelDeserializerFactory;
import phexstat.repository.EquipmentRepository;
import phexstat.repository.ExerciseRepository;
import phexstat.repository.WorkoutRepository;
import phexstat.repository.impl.EquipmentHashMapRepository;
import phexstat.repository.impl.ExerciseHashMapRepository;
import phexstat.repository.impl.WorkoutHashMapRepository;

import javax.xml.stream.XMLInputFactory;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * Created on 28.07.2017.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
@Configuration
//@ComponentScan(basePackages = "phexstat.repository")
public class ApplicationConfig {

    @Bean(destroyMethod = "destroy")
    public WorkoutRepository workoutRepository(@Value("#{systemEnvironment['DUMPS_DIR']}") File dumpsDirectoryFile) {
        return new WorkoutHashMapRepository(dumpsDirectoryFile);
    }

    @Bean
    public EquipmentRepository equipmentRepository(
            ModelDeserializerFactory deserializerFactory, @Value("classpath:equipment.xml") File equipmentXmlFile) {

        Map<Long, Equipment> preLoadedItems = new HashMap<>();
        try (ModelDeserializer<Equipment> deserializer =
                deserializerFactory.createEquipmentDeserializer(equipmentXmlFile)) {

            while (deserializer.hasNextTag()) {
                Equipment equipment = deserializer.nextModel();
                preLoadedItems.put(
                        Objects.requireNonNull(equipment.getId(), "cannot save entity without id"),
                        equipment
                );
            }
        } catch (Exception exc) {
            throw new RuntimeException(exc);
        }

        return new EquipmentHashMapRepository(preLoadedItems);
    }

    @Bean
    public ExerciseRepository exerciseRepository(
            ModelDeserializerFactory deserializerFactory, @Value("classpath:exercise.xml") File exerciseXmlFile) {

        Map<Long, Exercise> preLoadedItems = new HashMap<>();
        try (ModelDeserializer<Exercise> deserializer =
                     deserializerFactory.createExerciseDeserializer(exerciseXmlFile)) {

            while (deserializer.hasNextTag()) {
                Exercise exercise = deserializer.nextModel();
                preLoadedItems.put(
                        Objects.requireNonNull(exercise.getId(), "cannot save entity without id"),
                        exercise
                );
            }
        } catch (Exception exc) {
            throw new RuntimeException(exc);
        }

        return new ExerciseHashMapRepository(preLoadedItems);
    }

    @Bean
    public ModelDeserializerFactory modelDeserializerFactory() {
        return new ModelDeserializerFactory(new XmlMapper(), XMLInputFactory.newFactory());
    }

}
