package phexstat.repository;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import phexstat.repository.impl.HashMapCrudRepository;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * <p>
 * Created on 27.07.2017.
 * </p>
 *
 * <p>
 *     Test suite for {@link CrudRepository}
 * </p>
 *
 * @author Artemis A. Sirosh
 */
class HashMapCrudRepositoryTestSuite<T, ID> {

    void checkSave(T model, HashMapCrudRepository<T, ID> repository) {
        T saved = repository.save(model);
        Map<ID, T> innerStorageAfter = getInternalStorage(repository);

        assertTrue("Inner storage not contains saved item", innerStorageAfter.containsValue(saved));
    }

    void checkMultiSave(Iterable<T> models, HashMapCrudRepository<T, ID> repository) {
        Iterable<T> saved = repository.save(models);

        Map<ID, T> innerStorageAfter = getInternalStorage(repository);

        for (T modelSaved : saved) {
            assertTrue("Inner storage not contains saved item", innerStorageAfter.containsValue(modelSaved));
        }
    }

    void checkFind(ID key, T model, HashMapCrudRepository<T, ID> repository) {

        Map<ID, T> innerStorage = getInternalStorage(repository);
        if (innerStorage.get(key).equals(model))
            assertEquals(model, repository.findOne(key));
        else
            fail("Wrong test suite usage");
    }

    void checkMultiFind(Iterable<ID> ids, Iterable<T> models, HashMapCrudRepository<T, ID> repository) {

        Iterable<T> found = repository.findAll(ids);
        for (T model : models) {
            assertThat(found, CoreMatchers.hasItem(model));
        }
    }

    /**
     * Extracts internal storage
     * @param repository HashMap based repository
     * @return {@link Map} as internal database implementation
     */
    @SuppressWarnings("unchecked")
    private Map<ID, T> getInternalStorage(HashMapCrudRepository<T, ID> repository) {
        Map<ID, T> result;

        try {

            Field internalStorageField = HashMapCrudRepository.class.getDeclaredField("database");
            internalStorageField.setAccessible(true);
            result = (Map<ID, T>) internalStorageField.get(repository);
        } catch (NoSuchFieldException | IllegalAccessException exc) {
            throw new RuntimeException(exc);
        }

        return result;
    }
}
