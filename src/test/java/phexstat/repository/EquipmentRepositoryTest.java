package phexstat.repository;

import org.junit.Before;
import org.junit.Test;
import phexstat.model.Equipment;
import phexstat.repository.impl.EquipmentHashMapRepository;
import phexstat.repository.impl.HashMapCrudRepository;
import phexstat.util.Generators;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * <p>
 * Created on 27.07.2017.
 * </p>
 * <p>
 *     Test for {@link EquipmentRepository}
 * </p>
 * @author Artemis A. Sirosh
 */
public class EquipmentRepositoryTest {

    private Supplier<Equipment> generator = Generators.equipmentGenerator();
    private HashMapCrudRepositoryTestSuite<Equipment, Long> suite = new HashMapCrudRepositoryTestSuite<>();
    private EquipmentRepository repository;

    @Before
    public void setUp() throws Exception {
        this.repository = new EquipmentHashMapRepository();

    }

    @Test
    public void shouldSaveOneEquipment() throws Exception {

        Equipment equipment = generator.get();
        suite.checkSave(equipment, (HashMapCrudRepository) repository);
    }

    @Test
    public void shouldSaveSeveralEquipment() throws Exception {

        List<Equipment> equipments = Arrays.asList(generator.get(), generator.get());
        suite.checkMultiSave(equipments, (HashMapCrudRepository) repository);

    }

    @Test
    public void shouldFindOneEquipment() throws Exception {

        Equipment equipment = repository.save(generator.get());
        suite.checkFind(equipment.getId(), equipment,(HashMapCrudRepository) repository);

    }

    @Test
    public void shouldFindSeveralEquipments() throws Exception {

        Iterable<Equipment> equipments = repository.save(Arrays.asList(generator.get(), generator.get()));
        Iterable<Long> ids = StreamSupport.stream(equipments.spliterator(), false)
                .map(Equipment::getId)
                .collect(Collectors.toList());

        suite.checkMultiFind(ids, equipments, (HashMapCrudRepository) repository);

    }
}
