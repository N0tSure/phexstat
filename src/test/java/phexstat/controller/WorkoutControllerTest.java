package phexstat.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import phexstat.model.Exercise;
import phexstat.model.Workout;
import phexstat.repository.EquipmentRepository;
import phexstat.repository.ExerciseRepository;
import phexstat.repository.WorkoutRepository;
import phexstat.repository.impl.WorkoutHashMapRepository;
import phexstat.util.Generators;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * <p>
 * Created on 28.07.2017.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class WorkoutControllerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkoutControllerTest.class);

    @Test
    public void shouldReturnAllWorkouts() throws Exception {

        List<Workout> expectedWorkouts = Stream.generate(Generators.workoutGenerator())
                .limit(10)
                .collect(Collectors.toList());

        WorkoutRepository mockRepository = mock(WorkoutRepository.class);
        when(mockRepository.findAll()).thenReturn(expectedWorkouts);


        WorkoutController controller = new WorkoutController(null, null, mockRepository);
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        mockMvc.perform(get("/workouts"))
                .andExpect(view().name("/pages/workouts"))
                .andExpect(model().attributeExists("workouts"))
                .andExpect(model().attribute("workouts", hasItems(expectedWorkouts.toArray())))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldSaveWorkout() throws Exception {

        Workout expectedWorkout = new Workout(
                Generators.exerciseGenerator().get(), 3, 12,
                Generators.equipmentGenerator().get(), null);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(expectedWorkout);

        LOGGER.info("Expected workout: {}", json);

        ArgumentCaptor<Workout> workoutCaptor = ArgumentCaptor.forClass(Workout.class);
        WorkoutRepository workoutRepository = mock(WorkoutRepository.class);
        when(workoutRepository.save(workoutCaptor.capture())).thenReturn(expectedWorkout);

        ExerciseRepository exerciseRepository = mock(ExerciseRepository.class);
        when(exerciseRepository.findOne(any())).thenReturn(expectedWorkout.getExercise());

        EquipmentRepository equipmentRepository = mock(EquipmentRepository.class);
        when(equipmentRepository.findOne(any())).thenReturn(expectedWorkout.getEquipment());

        WorkoutController mockController = new WorkoutController(
                equipmentRepository,
                exerciseRepository,
                workoutRepository
        );

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(mockController).build();
        mockMvc.perform(post("/workouts")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        );

        assertThat(workoutCaptor.getValue().getExercise(), is(expectedWorkout.getExercise()));
        assertThat(workoutCaptor.getValue().getEquipment(), is(expectedWorkout.getEquipment()));
        assertThat(workoutCaptor.getValue().getApproaches(), is(expectedWorkout.getApproaches()));
        assertThat(workoutCaptor.getValue().getSet(), is(expectedWorkout.getSet()));

    }
}
