package phexstat.controller;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import phexstat.model.Exercise;
import phexstat.model.ExerciseType;
import phexstat.repository.ExerciseRepository;
import phexstat.repository.impl.ExerciseHashMapRepository;
import phexstat.util.Generators;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created on 04 Aug, 2017.
 *
 * <p>
 *     Test for {@link ExerciseController}
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class ExerciseControllerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExerciseControllerTest.class);

    @Test
    public void shouldReturnAllExercisesView() throws Exception {

        List<Exercise> expectedExercises = Stream.generate(Generators.exerciseGenerator())
                .limit(10)
                .collect(Collectors.toList());

        LOGGER.info("Expected exercises: {}", expectedExercises);

        ExerciseRepository mockRepository = mock(ExerciseRepository.class);
        when(mockRepository.findAll()).thenReturn(expectedExercises);

        ExerciseController controller = new ExerciseController(mockRepository);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        mockMvc.perform(get("/exercises"))
                .andExpect(status().isOk())
                .andExpect(view().name("/pages/exercises"))
                .andExpect(model().attribute("exercises", hasItems(expectedExercises.toArray())));


        verify(mockRepository, atLeastOnce()).findAll();

    }

    @Test
    public void shouldTakeExerciseToView() throws Exception {
        Exercise exercise = Generators.exerciseGenerator().get();
        Long exerciseId = 1L;
        LOGGER.info("Expected exercise: {}", exercise);

        ExerciseRepository mockRepository = mock(ExerciseRepository.class);
        when(mockRepository.findOne(exerciseId)).thenReturn(exercise);

        ExerciseController controller = new ExerciseController(mockRepository);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        mockMvc.perform(get("/exercises/" + String.valueOf(exerciseId)))
                .andExpect(status().isOk())
                .andExpect(view().name("/pages/exercise"))
                .andExpect(model().attribute("exercise", is(exercise)));

        verify(mockRepository, atLeastOnce()).findOne(exerciseId);

    }
}
