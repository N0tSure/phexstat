package phexstat.controller;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import phexstat.model.Equipment;
import phexstat.repository.EquipmentRepository;
import phexstat.util.Generators;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * <p>
 * Created on 10.08.2017.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class EquipmentControllerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(EquipmentControllerTest.class);

    @Test
    public void shouldReturnEquipment() throws Exception {
        List<Equipment> expectedEquipment = Stream.generate(Generators.equipmentGenerator())
                .limit(3)
                .collect(Collectors.toList());

        LOGGER.info("Expected equipment: {}", expectedEquipment);

        EquipmentRepository mockRepository = mock(EquipmentRepository.class);
        when(mockRepository.findAll()).thenReturn(expectedEquipment);

        EquipmentController controller = new EquipmentController(mockRepository);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/equipment"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()", is(expectedEquipment.size())))
                .andExpect(jsonPath("$.[0].name", is(expectedEquipment.get(0).getName())))
                .andExpect(jsonPath("$.[0].type", is(expectedEquipment.get(0).getType().name())))
                .andExpect(jsonPath("$.[0].measure", is(expectedEquipment.get(0).getMeasure())))
                .andExpect(jsonPath("$.[0].value", is(expectedEquipment.get(0).getValue())))
        ;

    }
}
