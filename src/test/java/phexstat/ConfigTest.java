package phexstat;

import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import phexstat.config.ApplicationConfig;
import phexstat.repository.EquipmentRepository;
import phexstat.repository.ExerciseRepository;
import phexstat.repository.WorkoutRepository;

import java.io.File;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * <p>
 * Created on 28.07.2017.
 * </p>
 * <p>
 *     Test for creation of simple Spring context, {@code ENVIRONMENT_VARIABLES}
 *     using there for set {@code DUMPS_DIR} variable for test.
 *     @see EnvironmentVariables
 *     {@code TEMPORARY_FOLDER} temporary folder and remove it after test,
 *     @see TemporaryFolder
 * </p>
 *
 * @author Artemis A. Sirosh
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public class ConfigTest {

    @ClassRule
    public static final EnvironmentVariables ENVIRONMENT_VARIABLES = new EnvironmentVariables();

    @ClassRule
    public static final TemporaryFolder TEMPORARY_FOLDER = new TemporaryFolder();

    @BeforeClass
    public static void setEnvironmentVariables() throws Exception {
        File temporal = TEMPORARY_FOLDER.newFolder();
        TEMPORARY_FOLDER.create();
        ENVIRONMENT_VARIABLES.set("DUMPS_DIR", temporal.getAbsolutePath());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        TEMPORARY_FOLDER.delete();
    }

    @Autowired
    private WorkoutRepository workoutRepository;

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Test
    public void beanCreationTest() throws Exception {
        assertNotNull(workoutRepository);
        assertNotNull(equipmentRepository);
        assertNotNull(exerciseRepository);

    }
}
