package phexstat.util;

import phexstat.model.*;

import java.util.Date;
import java.util.function.Supplier;

/**
 * Created on 14 Jun, 2017.
 *
 * <p>
 *     Utility for generation model instances
 *     suppliers for test purposes
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public final class Generators {
    private static final String[] WORDS =
            "foo bar baz qux quux corge grault garply waldo fred plugh xyzzy thud".split(" ");
    private static final Supplier<String> WORD_SUPPLIER = () ->
            WORDS[Math.round(((float) Math.random()  * (WORDS.length - 1)))];

    private Generators() {
        throw new UnsupportedOperationException("Wrong Usage");
    }

    /**
     * Creates supplier for {@link Workout}
     * @return {@link Supplier<Workout>}
     */
    public static Supplier<Workout> workoutGenerator() {
        return () -> new Workout(
                exerciseGenerator().get(),
                6, 3,
                equipmentGenerator().get(),
                new Date(Math.round(Math.random() * 1_000_000))
        );
    }

    /**
     * Creates supplier for {@link Exercise}
     * @return {@link Supplier<Exercise>}
     */
    public static Supplier<Exercise> exerciseGenerator() {
        return () -> new Exercise(
                WORD_SUPPLIER.get(),
                ExerciseType.BACK,
                EquipmentType.DUMBBELL,
                WORD_SUPPLIER.get(),
                WORD_SUPPLIER.get());
    }

    /**
     * Creates supplier for {@link Equipment}
     * @return {@link Supplier<Equipment>}
     */
    public static Supplier<Equipment> equipmentGenerator() {
        return () -> new Equipment(
                WORD_SUPPLIER.get(),
                EquipmentType.DUMBBELL,
                WORD_SUPPLIER.get(),
                Math.random()
        );
    }
}
