package phexstat.model.dumper;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import phexstat.model.Workout;
import phexstat.util.Generators;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * <p>
 * Created on 14.08.2017.
 * </p>
 *  <p>
 *      Test for {@link Dumper} implementation
 *      @see WorkoutCSVDumper
 *  </p>
 * @author Artemis A. Sirosh
 */
public class DumperTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DumperTest.class);

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void workoutCSVDumperTest() throws Exception {
        File temporalFolder = temporaryFolder.newFolder();

        List<Workout> workouts = Stream.generate(Generators.workoutGenerator()).limit(5).collect(Collectors.toList());
        LOGGER.info("Expected workouts: {}", workouts);

        try (Dumper<Workout> dumper = new WorkoutCSVDumper(temporalFolder.getAbsoluteFile())) {
            for (Workout workout : workouts) {
                dumper.dumpModel(workout);
            }
        }

        assertNotNull(temporalFolder.listFiles());
        assertThat(temporalFolder.listFiles(), arrayWithSize(1));
        File temp = temporalFolder.listFiles()[0];
        LOGGER.info("Found file: {}", temp);

        StringBuilder actual = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(temp))) {
            reader.lines().forEach(s -> actual.append(s).append('\n'));
        }

        for (Workout workout : workouts) {
            assertThat(actual.toString(), containsString(
                    String.format("%1$d,%2$td.%2$tm.%2$tY,%3$d,%4$d,%5$d,%6$d\n",
                            workout.getId(),
                            workout.getDate(),
                            workout.getExercise().getId(),
                            workout.getEquipment().getId(),
                            workout.getApproaches(),
                            workout.getSet()
            )));
        }
        LOGGER.info("\n{}", actual);
    }


}
