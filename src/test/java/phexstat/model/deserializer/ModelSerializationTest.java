package phexstat.model.deserializer;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import phexstat.model.Equipment;
import phexstat.model.EquipmentType;
import phexstat.model.Exercise;
import phexstat.model.ExerciseType;
import phexstat.util.Generators;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

/**
 * Created on 11 Aug, 2017.
 * <p>
 *     Test for {@link ModelDeserializer} implementations
 * </p>
 * @author Artemis A. Sirosh
 */
public class ModelSerializationTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelSerializationTest.class);

    @Test
    public void equipmentDeserializerTest() throws Exception {
        List<Equipment> equipmentList = new ArrayList<>();

        try (ModelDeserializer<Equipment> deserializer =
                new ModelDeserializerFactory(new XmlMapper(), XMLInputFactory.newFactory())
                        .createEquipmentDeserializer(new File("src/test/resources/test_equipment.xml"))) {

            while (deserializer.hasNextTag())
                equipmentList.add(deserializer.nextModel());
        }

        LOGGER.info("Deserialized equipment units: {}", equipmentList);
        assertThat(equipmentList, hasItems(
                new Equipment(
                        "Гантель 5.0",
                        EquipmentType.DUMBBELL,
                        "kg",
                        5.0
                ),
                new Equipment(
                        "Гантель 10.0",
                        EquipmentType.DUMBBELL,
                        "kg",
                        10.0
                )
        ));

    }

    @Test
    public void exerciseDeserializerTest() throws Exception {
        List<Exercise> exercises = new ArrayList<>();

       try (ModelDeserializer<Exercise> deserializer =
               new ModelDeserializerFactory(new XmlMapper(), XMLInputFactory.newFactory())
                       .createExerciseDeserializer(new File("src/test/resources/test_exercise.xml"))) {

           while (deserializer.hasNextTag())
               exercises.add(deserializer.nextModel());
       }

       LOGGER.info("Deserialized exercise units: {}", exercises);

       assertThat(exercises, hasItems(
               new Exercise(
               "Имя на русском №1",
                       ExerciseType.BACK,
                       EquipmentType.DUMBBELL,
                       "Описание на русском",
                       "plugh.jpg"),
               new Exercise(
                       "Имя на русском №2",
                       ExerciseType.CHEST,
                       EquipmentType.DUMBBELL,
                       "Описание на русском, другое, однако",
                       "baz.jpg"
               ))
       );
    }
}
