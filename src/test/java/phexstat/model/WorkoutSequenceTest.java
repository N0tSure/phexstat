package phexstat.model;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import phexstat.model.sequence.WorkoutSequence;
import phexstat.model.sequence.WorkoutSequenceFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 13 Jun, 2017.
 *
 * @author Artemis A. Sirosh
 */
public class WorkoutSequenceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkoutSequenceTest.class);

    private WorkoutSequenceFactory workoutSequenceFactory = new WorkoutSequenceFactory();

    @Test
    public void automaticLoadingSequenceTest() throws Exception {

        int expectedAmountOfExercise = 10;
        List<Exercise> actualExercises = new ArrayList<>();

        WorkoutSequence sequence = workoutSequenceFactory.getAutoLoadingSequence(expectedAmountOfExercise);

        assertEquals(sequence.currentExercise(), sequence.currentExercise());

        while (sequence.hasNextExercise())
            actualExercises.add(sequence.nextExercise());

        LOGGER.info("Exercises: {}", actualExercises);

        assertEquals(expectedAmountOfExercise, actualExercises.size());

        for (Exercise actualExercise : actualExercises) {
            assertNotNull(actualExercise);
        }
    }
}
